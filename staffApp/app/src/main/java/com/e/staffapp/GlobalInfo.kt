package com.e.staffapp

import android.util.Log
import android.widget.Toast
import androidx.core.util.lruCache
import com.e.staffapp.firebase.staffEmail
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class GlobalInfo {

    companion object {
        var PhoneNumber: String =""
        var emailId:String =""
        var latitude:Double = 0.0
        var longitude:Double = 0.0


//        var MyTrackers: MutableMap<String, String> = HashMap()


        fun UpdatesInfo(UserPhone: String) {
            val df = SimpleDateFormat("yyyy/MM/dd HH:MM:ss")
            val date = Date()
            val database = FirebaseDatabase.getInstance().reference
            database.child("Staffs").child(UserPhone).child("Timestamp").setValue(df.format(date).toString())
            database.child("Staffs").child(UserPhone).child("EmailId").setValue(emailId)

        }

        fun UpdateLocation() {

            val saveloct = staffEmail()
            val df = SimpleDateFormat("yyyy/MM/dd HH:MM:ss")
            val date = Date()

            val database = FirebaseDatabase.getInstance().reference
            database.child("Staffs").child(PhoneNumber).child("Timestamp").setValue(df.format(date).toString())
            database.child("Staffs").child(PhoneNumber).child("Location").child("Latitude").setValue(latitude)
            database.child("Staffs").child(PhoneNumber).child("Location").child("Longitude").setValue(longitude)

//            Log.i("savenum", PhoneNumber)
        }


    }



}


