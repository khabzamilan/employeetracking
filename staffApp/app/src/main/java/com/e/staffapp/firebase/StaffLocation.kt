package com.e.staffapp.firebase

class StaffLocation(val date: String, val lat:Double, val lon: Double ) {

    constructor(): this("",0.0,0.0)
}