package com.e.staffapp.map

import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address

import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.e.staffapp.GlobalInfo
import com.e.staffapp.R
import com.e.staffapp.firebase.staffEmail
import com.e.staffapp.login
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_map_page.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.log


class mapPage : AppCompatActivity(), OnMapReadyCallback,
            GoogleMap.OnMarkerClickListener
        {
            companion object{
                private const val LOCATION_PERMISION_REQUEST_CODE = 1

                private const val REQUEST_CHECK_SETTINGS = 2
            }



        override fun onMarkerClick(p0: Marker?) = false


            private lateinit var map: GoogleMap
            private  lateinit var  fusedLocationClient: FusedLocationProviderClient
            private lateinit var lastLocation: Location

            private lateinit var locationCallback: LocationCallback
            private lateinit var locationRequest: LocationRequest
            private var locationUpdateState = false

           private lateinit var ref: DatabaseReference
            private lateinit var auth: FirebaseAuth
//            lateinit var editTextPhoneNumber: EditText

            lateinit var editTextStaffNumber:EditText





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_page)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)


        ref = FirebaseDatabase.getInstance().getReference("Staffs")
        auth = FirebaseAuth.getInstance()

        editTextStaffNumber = findViewById(R.id.staffNumberCache)



// Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        locationCallback = object: LocationCallback(){
            override fun onLocationResult(p0: LocationResult){
                super.onLocationResult(p0)

                lastLocation = p0.lastLocation
                placeMarkerOnMap((LatLng(lastLocation.latitude, lastLocation.longitude)))
            }
        }


        createLocationRequest()


        logoutButton.setOnClickListener{
            auth.signOut()
            startActivity(Intent(this, login::class.java))
            finish()
        }


    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.uiSettings.isZoomControlsEnabled = true
        map.setOnMarkerClickListener(this)

        setUpMap()






    }




            private fun setUpMap(){

                if (ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(this,
                        arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                        LOCATION_PERMISION_REQUEST_CODE
                    )
                    return
                }

                map.isMyLocationEnabled=true

                fusedLocationClient.lastLocation.addOnSuccessListener(this){ location ->

                    if (location !=null){
                        lastLocation = location
                        val currentLatLng = LatLng(location.latitude, location.longitude)
                        placeMarkerOnMap(currentLatLng)



                        saveLocation(currentLatLng)

                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,10f))
                    }
                }




            }


            private fun saveLocation(LatLng: LatLng) {
//                var phonNumber = GlobalInfo.PhoneNumber.toString()
                    val df = SimpleDateFormat("yyyy/MM/dd HH:MM:ss")
                val date = Date()
                var time = df.format(date).toString()
//                val Emailid = GlobalInfo.emailId
                readCache()

                val phonNumber = staffNumberCache.text.toString()
                GlobalInfo.PhoneNumber = phonNumber
                GlobalInfo.latitude = LatLng.latitude
                GlobalInfo.longitude = LatLng.longitude


                GlobalInfo.UpdateLocation()



//                val saveStaffLocation = staffEmail(time, LatLng.latitude,LatLng.longitude)
//
//                ref.child(phonNumber).setValue(saveStaffLocation).addOnCompleteListener {
//                    Toast.makeText(applicationContext, "Data Inserted", Toast.LENGTH_LONG).show()
//                }
            }

            val file_name= "numberFile"


            fun readCache(){

                try{
                    val numberRead = File(cacheDir,file_name)
                    val fileInputStream = FileInputStream(numberRead)
                    val inputStreamReader = InputStreamReader(fileInputStream)
                    val bufferedReader = BufferedReader(inputStreamReader)
                    val stringBuilder = StringBuilder()
                    var line:String? = null
                    while ({line = bufferedReader.readLine();line}()!=null)
                    {
                        stringBuilder.append(line)
                    }
                    fileInputStream.close()
                    staffNumberCache.setText(stringBuilder.toString())
                    Toast.makeText(applicationContext,"data read",Toast.LENGTH_LONG).show()
                }catch (exp : java.io.IOException){
                    exp.printStackTrace()
                }

            }

     private fun placeMarkerOnMap(location: LatLng){

         val markerOptions = MarkerOptions().position(location)
         val titleStr = getAddress(location)

         markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))

         markerOptions.title(titleStr)
         map.clear()

         map.addMarker(markerOptions)


     }

//





            //geocoding implementation
            private fun getAddress(latLng: LatLng): String {

                val geocoder = Geocoder(this)
                val addresses: List<Address>?
                val address: Address?
                var addressText = ""

                Log.i("locate", latLng.toString())

                try {



                    addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
//                    Log.i("address", addresses.toString())
                    if (null != addresses && addresses.isNotEmpty()) {
                        address = addresses[0]
                        for (i in 0 until address.maxAddressLineIndex) {
                            addressText += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(i)
                        }
                    }
                } catch (e: IOException) {
                    Log.e("mapPage", e.localizedMessage)
                }
                Log.i("address",addressText)
                return addressText
            }


    private fun startLocationUpdates(){
        if(ActivityCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED
                ){
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISION_REQUEST_CODE
            )
            return
        }

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }



    private fun createLocationRequest(){

        locationRequest = LocationRequest()
        locationRequest.interval = 10000

        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }

        task.addOnFailureListener { e ->
            // 6
            if (e is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    e.startResolutionForResult(this@mapPage,
                        REQUEST_CHECK_SETTINGS
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
        }
        }
    }


            override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)  {
                super.onActivityResult(requestCode, resultCode, data)
                if (requestCode == REQUEST_CHECK_SETTINGS) {
                    if (resultCode == Activity.RESULT_OK) {
                        locationUpdateState = true
                        startLocationUpdates()
                    }
                }
            }

    override fun onPause(){
        super.onPause()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    public override fun onResume(){
        super.onResume()
        if(!locationUpdateState){
            startLocationUpdates()
        }
    }


}
