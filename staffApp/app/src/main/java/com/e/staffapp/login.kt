package com.e.staffapp

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.Patterns
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.e.staffapp.map.mapPage
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_login.*
import java.io.*


class login : AppCompatActivity() {
    
    private lateinit var loginBtn:Button
    private lateinit var auth: FirebaseAuth
    private lateinit var ref:DatabaseReference
    lateinit var textEmail: EditText
    lateinit var textPassword: EditText
    lateinit var phoneNumber: EditText



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        auth = FirebaseAuth.getInstance()
        ref = FirebaseDatabase.getInstance().getReference("Staffs")


        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        val message = dialogView.findViewById<TextView>(R.id.message)
        message.text = "Loading. Please wait..."

        textEmail = findViewById(R.id.loginEmail)
        textPassword = findViewById(R.id.loginPassword)
        phoneNumber = findViewById(R.id.loginPhoneNumber)


        loginBtn = findViewById(R.id.loginBtn)

        loginBtn.setOnClickListener{



            builder.setView(dialogView)
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.show()

//            Handler().postDelayed({dialog.dismiss()},1000)
            loginFun()
        }
    }
    val file_name= "numberFile"
    var strData:String = ""

    fun saveCache(){
        try{
            val numberSave = File(cacheDir, file_name)
            val fileOutputStream = FileOutputStream(numberSave)
            strData = phoneNumber.text.toString()
            fileOutputStream.write(strData.toByteArray())
            fileOutputStream.close()
            Toast.makeText(applicationContext,"data is saved in cache", Toast.LENGTH_LONG).show()
            phoneNumber.setText("")
        }catch (exp:java.io.IOException){
            exp.printStackTrace()
        }
        Log.i("loginnumber", strData)
    }
//    fun readCache(){
//
//        try{
//            val numberRead = File(cacheDir,file_name)
//            val fileInputStream = FileInputStream(numberRead)
//            val inputStreamReader = InputStreamReader(fileInputStream)
//            val bufferedReader = BufferedReader(inputStreamReader)
//            val stringBuilder = StringBuilder()
//            var line:String? = null
//            while ({line = bufferedReader.readLine();line}()!=null)
//            {
//                stringBuilder.append(line)
//            }
//            fileInputStream.close()
//
//            phoneNumber.setText(stringBuilder.toString())
//            Toast.makeText(applicationContext,"data read",Toast.LENGTH_LONG).show()
//        }catch (exp : java.io.IOException){
//            exp.printStackTrace()
//        }
//    }


    private fun loginFun(){


        Log.i("phoneNumber", phoneNumber.toString())

        if (loginEmail.text.toString().isEmpty()) {
            loginEmail.error = "Please enter email"
            loginEmail.requestFocus()
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(loginEmail.text.toString()).matches()) {
            loginEmail.error = "Please enter valid email"
            loginEmail.requestFocus()
            return
        }
        if (loginPassword.text.toString().isEmpty()) {
            loginPassword.error = "Please enter password"
            loginPassword.requestFocus()
            return
        }


        auth.signInWithEmailAndPassword(loginEmail.text.toString(), loginPassword.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {

                    val num = phoneNumber.text.toString()
                    val staffEmail = loginEmail.text.toString()
//                    val addStaff1 = loginEmail.text.toString().trim()
                    GlobalInfo.emailId = staffEmail
                    GlobalInfo.PhoneNumber = num
                    GlobalInfo.UpdatesInfo(GlobalInfo.PhoneNumber!!)

                    saveCache()

                    val user = auth.currentUser
                    updateUI(user)
                } else {

                    updateUI(null)
                }

                // ...
            }


    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        updateUI(currentUser)

    }

    private fun updateUI(currentUser: FirebaseUser?) {
        if (currentUser != null) {

            startActivity(Intent(this@login, mapPage::class.java))
            finish()
        } else {
            Toast.makeText(
                baseContext, "Login failed.",
                Toast.LENGTH_SHORT
            ).show()
        }

    }


}