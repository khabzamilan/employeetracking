package com.e.adminapp.activities

import com.google.firebase.database.FirebaseDatabase
import java.text.SimpleDateFormat
import java.util.*

class GlobalInfo {

    companion object {
        var EmailId: String? = ""
//        var MyTrackers: MutableMap<String, String> = HashMap()

        fun UpdatesInfo(StaffEmail: String) {
            val df = SimpleDateFormat("yyyy/MM/dd HH:MM:ss")
            val date = Date()
            val mDatabase = FirebaseDatabase.getInstance().reference
            mDatabase.child("Staffs").child(StaffEmail).child("Updates")
                .setValue(df.format(date).toString())
        }
    }


}