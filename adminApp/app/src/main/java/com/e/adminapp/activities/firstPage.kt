package com.e.adminapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.e.adminapp.R
import com.e.adminapp.map.adminMap
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_existing_user.*

class firstPage : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var btnNewUser:Button
    private lateinit var btnExistingUser:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firstpage)

        auth = FirebaseAuth.getInstance()

        btnNewUser=findViewById(R.id.btnNewUser)
        btnExistingUser= findViewById(R.id.btnExistingUser)

        checkLoginOrNot()

        btnNewUser.setOnClickListener {
            bNewUser()
        }

        btnExistingUser.setOnClickListener{
            bExistingUser()
        }

    }

    fun checkLoginOrNot(){
//        auth.signInWithEmailAndPassword(loginEmail.text.toString(), loginPassword.text.toString())
//            .addOnCompleteListener(this) { task ->

//                if (task.isSuccessful) {
//                    val user = auth.currentUser
//                    updateUI(user)
//                } else {
//
//                    updateUI(null)
//                }
//
//                // ...
//            }
        val user = auth.currentUser

        if (user!=null){
            startActivity(Intent(this@firstPage,adminMap::class.java))
            finish()
        }
        else {
            Toast.makeText(
                baseContext, "Login failed.",
                Toast.LENGTH_SHORT
            ).show()
        }


    }

//    private fun updateUI(currentUser: FirebaseUser?) {
//        if (currentUser != null) {
//
//            startActivity(Intent(this@firstPage,adminMap::class.java))
//            finish()
//        } else {
//            Toast.makeText(
//                baseContext, "Login failed.",
//                Toast.LENGTH_SHORT
//            ).show()
//        }

//    }



    private fun bNewUser(){
        val intent =Intent(this@firstPage,newUser::class.java)
        startActivity(intent)
    }


    private fun bExistingUser(){
        val intent =Intent(this@firstPage,existingUser::class.java)
        startActivity(intent)
    }



}
