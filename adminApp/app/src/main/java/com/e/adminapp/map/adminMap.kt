package com.e.adminapp.map

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.Patterns
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.e.adminapp.R
import com.e.adminapp.activities.GlobalInfo
import com.e.adminapp.activities.existingUser
import com.e.adminapp.firebase.AddNewStaffs
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_admin_map.*
import kotlinx.android.synthetic.main.activity_new_user.*
import kotlinx.android.synthetic.main.addnewstaff.*
import java.io.IOException

class adminMap : AppCompatActivity(), OnMapReadyCallback ,
    GoogleMap.OnMarkerClickListener
{
    companion object{
        private const val LOCATION_PERMISION_REQUEST_CODE = 1

        private const val REQUEST_CHECK_SETTINGS = 2
    }

    override fun onMarkerClick(p0: Marker?) = false


    private lateinit var map: GoogleMap
    private  lateinit var  fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location

    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private var locationUpdateState = false
    private lateinit var auth: FirebaseAuth

    private lateinit var ref: DatabaseReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_map)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        auth = FirebaseAuth.getInstance()
        ref = FirebaseDatabase.getInstance().getReference("Staffs")

        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        val message = dialogView.findViewById<TextView>(R.id.message)
        message.text = "Loading. Please wait..."



        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        locationCallback = object: LocationCallback(){
            override fun onLocationResult(p0: LocationResult){
                super.onLocationResult(p0)

                lastLocation = p0.lastLocation
                placeMarkerOnMap((LatLng(lastLocation.latitude, lastLocation.longitude)))
            }
        }


        createLocationRequest()

        //adding new users

        addStaffButton.setOnClickListener{
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Add New Staffs")
            builder.setView(dialogView)
            builder.setCancelable(false)
            val dialog = builder.create()

            val view = layoutInflater.inflate(R.layout.addnewstaff,null)
            val username = view.findViewById<EditText>(R.id.addNSEmail)
            val password = view.findViewById<EditText>(R.id.addNSPassword)

            builder.setView(view)
            builder.setPositiveButton("Add"){_,_->
                signupNewStaff(username, password, dialog)
            }
            builder.setNegativeButton("Close"){_,_->

            }
            builder.show()

        }


    }


    private fun signupNewStaff(username:EditText, password:EditText, dialog:AlertDialog){



        if (username.text.toString().isEmpty()){
            Toast.makeText(baseContext,"Enter the Email",Toast.LENGTH_SHORT).show()

            return
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(username.text.toString()).matches()){
            Toast.makeText(baseContext,"Enter the valid Email",Toast.LENGTH_SHORT).show()

            return
        }

        if (password.text.toString().isEmpty()) {
            Toast.makeText(baseContext,"Enter the Password",Toast.LENGTH_SHORT).show()

            return
        }
        var addStaffEmailid = username.text.toString().trim()
        val staffId = ref.push().key!!



        auth.createUserWithEmailAndPassword(username.text.toString(), password.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {

                    ref.child(staffId).setValue(AddNewStaffs(addStaffEmailid)).addOnCompleteListener {
                        Toast.makeText(applicationContext, "added",Toast.LENGTH_SHORT).show()
                    }
                    Handler().postDelayed({dialog.dismiss()},5000)
                    dialog.show()
                    Toast.makeText(baseContext,"Staff adding success.",Toast.LENGTH_LONG).show()



//                    GlobalInfo.EmailId = username.text.toString()
//                    GlobalInfo.UpdatesInfo(GlobalInfo.EmailId!!)

                    startActivity(Intent( this, adminMap::class.java))

                    finish()

                } else {
                    Toast.makeText(baseContext, "Staff adding failed. Please enter password length greater than 6 and try again later",
                        Toast.LENGTH_LONG).show()

                }


            }



    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.uiSettings.isZoomControlsEnabled = true
        map.setOnMarkerClickListener(this)

        setUpMap()






    }




    private fun setUpMap(){

        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISION_REQUEST_CODE)
            return
        }

        map.isMyLocationEnabled=true

        fusedLocationClient.lastLocation.addOnSuccessListener(this){ location ->

            if (location !=null){
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                placeMarkerOnMap(currentLatLng)

                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng,10f))
            }
        }


    }

    private fun placeMarkerOnMap(location: LatLng){

        val markerOptions = MarkerOptions().position(location)
//        val titleStr = getAddress(location)
//        markerOptions.title(titleStr)
        map.clear()
        map.addMarker(markerOptions)
    }


    //geocoding implementation
//    private fun getAddress(latLng: LatLng): String {
//
//        val geocoder = Geocoder(this)
//        val addresses: List<Address>?
//        val address: Address?
//        var addressText = ""
//
//        try {
//
//            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
//
//            if (null != addresses && addresses.isNotEmpty()) {
//                address = addresses[0]
//                for (i in 0 until address.maxAddressLineIndex) {
//                    addressText += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(i)
//                }
//            }
//        } catch (e: IOException) {
//            Log.e("adminMap", e.localizedMessage)
//        }
//
//        return addressText
//    }


    private fun startLocationUpdates(){
        if(ActivityCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED
        ){
            ActivityCompat.requestPermissions(this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISION_REQUEST_CODE)
            return
        }

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }



    private fun createLocationRequest() {
        // 1
        locationRequest = LocationRequest()
        // 2
        locationRequest.interval = 10000
        // 3
        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)

        // 4
        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(builder.build())

        // 5
        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }
        task.addOnFailureListener { e ->
            // 6
            if (e is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    e.startResolutionForResult(this@adminMap,
                        REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                locationUpdateState = true
                startLocationUpdates()
            }
        }
    }

    override fun onPause(){
        super.onPause()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    public override fun onResume(){
        super.onResume()
        if(!locationUpdateState){
            startLocationUpdates()
        }
    }
}
