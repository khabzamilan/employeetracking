package com.e.adminapp.activities

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Patterns
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.e.adminapp.R
import com.e.adminapp.map.adminMap
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_new_user.*

class newUser : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_user)

        auth = FirebaseAuth.getInstance()
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        val message = dialogView.findViewById<TextView>(R.id.message)
        message.text = "Loading. Please wait..."


        btnNewUserSignup.setOnClickListener{

            builder.setView(dialogView)
            builder.setCancelable(false)
            val dialog = builder.create()



            signupUser(dialog)
        }
    }


    private fun signupUser(dialog: AlertDialog){
        if (emailid.text.toString().isEmpty()){
            emailid.error = " Please enter Email"
            emailid.requestFocus()
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(emailid.text.toString()).matches()){
            emailid.error = " Please enter valid email"
            emailid.requestFocus()
            return
        }

        if (password.text.toString().isEmpty()){
            password.error = "Please enter password"
            password.requestFocus()
            return
        }

        auth.createUserWithEmailAndPassword(emailid.text.toString(), password.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    dialog.show()
//                    Handler().postDelayed({dialog.dismiss()},5000)
                    startActivity(Intent( this,existingUser::class.java))
                    finish()

                } else {
                    Toast.makeText(baseContext, "Sign up failed. Please enter password length greater than 6 and try again later",
                        Toast.LENGTH_SHORT).show()

                }


            }
    }


    }

