package com.e.adminapp.activities

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.e.adminapp.R
import com.e.adminapp.map.adminMap
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_existing_user.*
import kotlinx.android.synthetic.main.activity_new_user.*

class existingUser : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    lateinit var textEmail: EditText
    lateinit var textPassword: EditText
    lateinit var btnLogin: Button
    lateinit var dontHaveAcc: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_existing_user)

        auth = FirebaseAuth.getInstance()

        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        val message = dialogView.findViewById<TextView>(R.id.message)
        message.text = "Loading. Please wait..."

        textEmail = findViewById(R.id.loginEmail)
        textPassword = findViewById(R.id.loginPassword)
        dontHaveAcc = findViewById((R.id.dontHaveAcc))

        btnLogin = findViewById(R.id.btnLogin)
        btnLogin.setOnClickListener {
            builder.setView(dialogView)
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.show()
//            Handler().postDelayed({dialog.dismiss()},5000)

            logIn()
        }

        dontHaveAcc.setOnClickListener {
            signupFun()
        }
    }


    private fun signupFun() {
        val intent = Intent(this@existingUser, newUser::class.java)
        startActivity(intent)
    }

    private fun logIn() {
        if (loginEmail.text.toString().isEmpty()) {
            loginEmail.error = "Please enter email"
            loginEmail.requestFocus()
            return
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(loginEmail.text.toString()).matches()) {
            loginEmail.error = "Please enter valid email"
            loginEmail.requestFocus()
            return
        }
        if (loginPassword.text.toString().isEmpty()) {
            loginPassword.error = "Please enter password"
            loginPassword.requestFocus()
            return
        }


        auth.signInWithEmailAndPassword(loginEmail.text.toString(), loginPassword.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {



                    val user = auth.currentUser
                    updateUI(user)
                } else {

                    updateUI(null)
                }

                // ...
            }


    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        updateUI(currentUser)

    }

    private fun updateUI(currentUser: FirebaseUser?) {
        if (currentUser != null) {

                startActivity(Intent(this@existingUser,adminMap::class.java))
                finish()
            } else {
                Toast.makeText(
                    baseContext, "Login failed.",
                    Toast.LENGTH_SHORT
                ).show()
            }

        }
    }




